/**
 * 1. Напиши функцию createPromise, которая будет возвращать промис
 * */

function createPromise () {

}

/**
 * 2. Напиши функцию createResolvedPromise, которая будет возвращать промис, который успешно выполнен (fulfilled)
 * */

function createResolvedPromise () {

}

/**
 * 3. Напиши функцию createResolvedPromiseWithData, которая будет возвращать промис, который успешно выполнен
 * (fulfilled) и возвращать при резолве строчку "Готово!"
 * */

function createResolvedPromiseWithData () {

}

/**
 * 4. Напиши функцию createRejectedPromise, которая будет возвращать промис, который будет отклонен (rejected)
 * */

function createRejectedPromise () {

}

/**
 * 5. Напиши функцию createRejectedPromiseWithError, которая будет возвращать промис, который будет отклонен
 * (rejected) и отклоненыый промис должен возвращать объект ошибки с текстом "Что-то пошло не так"
 * */

function createRejectedPromiseWithError () {

}

/**
 * 6. Напиши функцию fulfilledOrNotFulfilled, которая принимает на вход boolean аргумент и возвращает промис.
 * Если аргумент true, то промис должен быть выполнен, иначе — отклонен.
 * */

function fulfilledOrNotFulfilled (shouldBeResolve) {

}

/**
 * 7. Создай функцию timer, которая возвращает промис, который выполняется через 3 секунды.
 * */

function timer () {

}

module.exports = {
    createPromise,
    createResolvedPromise,
    createResolvedPromiseWithData,
    createRejectedPromise,
    createRejectedPromiseWithError,
    fulfilledOrNotFulfilled,
    timer,
};
